import './App.css';
import Teste from './Teste';
import Teste2 from './Teste2';

function App() {
  return (
    <div className="container">
      <header className="header">
        <h1 className="title">
          Formulário de Cadastro
        </h1>
        <form>
          <div className="form-group">
            <label>Nome</label>
            <input type="text" name="name" />
          </div>
          <div className="form-group">
            <label>Idade</label>
            <input type="number" name="age" />
          </div>
          <div className="form-group">
            <label>E-mail</label>
            <input type="email" name="email" />
          </div>
          <div className="form-group">
            <label>Possui Acessibilidade?</label>
            <div className="radio-group">
              <input type="radio" name="accessibility" value="true" /> Sim
              <input type="radio" name="accessibility" value="false" /> Não
            </div>
          </div>
          <button type="submit" className="btn-submit">Realizar Cadastro</button>
          <hr></hr>
          <Teste />
          <hr></hr>
          <Teste2 />
        </form>
      </header>
    </div>
  );
}

export default App;
