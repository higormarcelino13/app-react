import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Teste from './Teste';
import Teste2 from './Teste2';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <App />
    <Teste />
    <Teste2 />
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
